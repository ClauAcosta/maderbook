
package com.polo.MaderBook.entidades;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true, exclude = "usuario")
@Entity
@Table(name = "cliente")
@JsonInclude(JsonInclude.Include.ALWAYS)
public class Cliente extends Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String telefono;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

    public Cliente(String email) {
        this.email = email;
    }

    @Override
    public String getDatos() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    /*metodo de prueba de impresion de todas las columnas.*/
    public void imprimirDatos(){
        System.out.println("ID: " + id);
        System.out.println("email: " + email);
        System.out.println("telefono: " + telefono);
        System.out.println("nombre: " + nombre);
        System.out.println("provincia: " + provincia);
        System.out.println("cuit: " + cuit);
        System.out.println("provincia: "+ provincia);
        System.out.println("ciudad: " +ciudad);
        System.out.println("condicionFiscal " + condicionFiscal);
        System.out.println("fechaAlta: "  +fechaAlta);

    }

    /*CHEQUEAR TO STRING tuve que cambiar porque me tiraba un loop y hacia stackoverflow*/
    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", telefono='" + telefono + '\'' +
                ", usuario=" + usuario +
                '}';
    }
}
