
package com.polo.MaderBook.entidades;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDate;

import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@MappedSuperclass
@JsonInclude(JsonInclude.Include.ALWAYS)
public abstract class Persona {
    
    
    protected String nombre;
    protected String cuit;
    protected String domicilio;
    protected String provincia;
    protected String ciudad;
    protected String condicionFiscal;
    protected LocalDate fechaAlta = LocalDate.now();


    public abstract String getDatos();
    
    
}

