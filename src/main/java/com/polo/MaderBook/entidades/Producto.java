
package com.polo.MaderBook.entidades;

import jakarta.persistence.Entity;
import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@MappedSuperclass
public abstract class Producto {
    

    protected String material;
    protected double precio;
    protected boolean disponibilidad;
    protected String descripcion;

    public Producto(String material){
        this.material = material;
    }
    public abstract String getProducto();
}
