
package com.polo.MaderBook.entidades;

import com.polo.MaderBook.repositorios.ProdMayoristaRepositorio;
import jakarta.persistence.*;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Usuario extends Persona {

    // Debemos lograr que el Usuario pueda manipular TODOS los metodos de
    // la clase producto
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String password;

@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
@JoinTable(
    name = "usuarios_roles",
    joinColumns = @JoinColumn(name = "usuario_id", referencedColumnName = "id"),inverseJoinColumns = @JoinColumn(name = "rol_id", referencedColumnName = "id")
)
    private Collection<Rol> roles;

    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ProdMayorista> productos = new ArrayList<>();
    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Cliente> clientes;


    public Usuario(String nombre, String cuit, String domicilio, String provincia, String ciudad,
            String condicionFiscal, LocalDate fechaAlta, Long id, String email, String password,
            Collection<Rol> roles, List<ProdMayorista> productos, List<Cliente> clientes) {
        super(nombre, cuit, domicilio, provincia, ciudad, condicionFiscal, fechaAlta);
        this.id = id;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.productos = productos;
        this.clientes = clientes;
    }

    public Usuario(String nombre, String cuit, String domicilio, String provincia, String ciudad,
            String condicionFiscal, LocalDate fechaAlta, String email, String password, Collection<Rol> roles,
            List<ProdMayorista> productos, List<Cliente> clientes) {
        super(nombre, cuit, domicilio, provincia, ciudad, condicionFiscal, fechaAlta);
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.productos = productos;
        this.clientes = clientes;
    }


    @Override
    public String getDatos() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from
                                                                       // nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    /*
     * CHEQUEAR TO STRING tuve que cambiar porque me tiraba un loop y hacia
     * stackoverflow
     */
    @Override
    public String toString() {
        return "Usuario{" +
                "\nId= " + id +
                "\nNombre= " + nombre +
                "\nCUIT= " + cuit +
                "\nDomicilio= " + domicilio +
                "\nProvincia= " + provincia +
                "\nCiudad = " + ciudad +
                "\nCondicion Fiscal= " + condicionFiscal +
                "\nFecha de alta= " + fechaAlta +
                "\nemail= " + email + '\'' +
                "\ncontrasena= '" + password + '\'' +
                '}';
    }

}
