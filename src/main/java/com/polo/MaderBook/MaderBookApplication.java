package com.polo.MaderBook;

import com.polo.MaderBook.entidades.Cliente;
import com.polo.MaderBook.entidades.ProdMayorista;
import com.polo.MaderBook.entidades.Usuario;
import com.polo.MaderBook.servicios.ProdMayoristaServicios;
import com.polo.MaderBook.servicios.UsuarioServicio;

import java.time.LocalDate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import com.polo.MaderBook.servicios.ClienteServicios;


@SpringBootApplication
public class MaderBookApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(MaderBookApplication.class, args);
		/* ClienteServicios clienteServicios = context.getBean(ClienteServicios.class);
		ProdMayoristaServicios mayServicios = context.getBean(ProdMayoristaServicios.class); */
		UsuarioServicio usServicios = context.getBean(UsuarioServicio.class);

		 /* try {
		 Cliente cliente = new Cliente();
		 cliente.setCiudad(null);
		 cliente.setCondicionFiscal(null);
		 cliente.setDomicilio(null);
		 cliente.setEmail(null);
		 cliente.setProvincia(null);
		 cliente.setFechaAlta(LocalDate.now());
		 cliente.setUsuario(null);
		 cliente.setTelefono(null);
		 
		 Cliente clienteGuardado = clienteServicios.guardarCliente(cliente);
		 System.out.println("Cliente guardado con éxito. ID: " +
		 clienteGuardado.getId());
		 cliente.imprimirDatos();
		 } catch (Exception e) {
		 System.err.println("Error al guardar el cliente: " + e.getMessage());
		 } */
//		 try {
//		 ProdMayorista may = new ProdMayorista(100, "Pino");
//		 ProdMayorista mayGuardado = mayServicios.guardarProdMayorista(may);
//		 System.out.println("Mayorista guardado con éxito. ID: " +
//		 mayGuardado.getId());
//		 } catch (Exception e) {
//		 System.err.println("Error al guardar el may: " + e.getMessage());
//		 }

//		Long productoId = 4L;
//		try {
//			mayServicios.aumentarStock(productoId, 100);
//			System.out.println("Stock aumentado en 100 unidades.");
//		} catch (Exception e) {
//			System.err.println("Error al aumentar el stock: " + e.getMessage());
//		}

		/* Long productoId = 4L;
		try {
			mayServicios.reducirStock(productoId, 100);
			System.out.println("Stock aumentado en 100 unidades.");
		} catch (Exception e) {
			System.err.println("Error al aumentar el stock: " + e.getMessage());
		} */


		// Reducir el stock en 50 unidades
//		try {
//			mayServicios.reducirStock(productoId, 50);
//			System.out.println("Stock reducido en 50 unidades.");
//		} catch (Exception e) {
//			System.err.println("Error al reducir el stock: " + e.getMessage());
//		}

		// Imprimir el estado actual del producto

//		ProdMayorista productoActual = mayServicios.getProdMayoristaPorId(productoId);
//		System.out.println("Estado actual del producto:");
//		System.out.println("ID: " + productoActual.getId());
//		System.out.println("Stock: " + productoActual.getStock());
	//	System.out.println(mayServicios.getTodoMayorista());

//		 try {
//		 Usuario usuario = new Usuario();
//		 Usuario usGuardado = usServicios.guardarUsuario(usuario);
//		 System.out.println("Usuario guardado con éxito. ID: " + usGuardado.getId());
//		 } catch (Exception e) {
//		 System.err.println("Error al guardar el usuario: " + e.getMessage());
//		 }

		/* usServicios.agregarProductoAUsuario(10L, mayServicios.getProdMayoristaPorId(5L));
		System.out.println(mayServicios.getProdMayoristaPorId(5L));
		System.out.println("Impresion de metodo de usuario get producto");
		System.out.println(usServicios.getProductosDeUsuario(10L)); */
//		usServicios.eliminarProductoDeUsuario(10L, 5L);
//		System.out.println(mayServicios.getProdMayoristaPorId(5L));


		/* ProdMayorista productoActual = mayServicios.getProdMayoristaPorId(productoId);
		System.out.println("Estado actual del producto:");
		System.out.println("ID: " + productoActual.getId());
		System.out.println("Stock: " + productoActual.getStock());
		System.out.println(mayServicios.getTodoMayorista()); */
		// try {
		// Usuario usuario = new Usuario();
		// Usuario usGuardado = usServicios.guardarUsuario(usuario);
		// System.out.println("Usuario guardado con éxito. ID: " + usGuardado.getId());
		// } catch (Exception e) {
		// System.err.println("Error al guardar el usuario: " + e.getMessage());
		// }
		// clienteServicios.eliminarClientes();
		// System.out.println(clienteServicios.getClientes());
		// System.out.println(clienteServicios.getClientePorId(5L));
		// clienteServicios.eliminarClientePorId(17L);
		// System.out.println("------------INICIO---------");
		// Usuario usuario = usServicios.getUsuarioPorId(1L);
		// Cliente nuevoCliente = new Cliente("lolo@gmail.com");
		// nuevoCliente.setUsuario(usuario);
		// clienteServicios.guardarCliente(nuevoCliente);
		// System.out.println(usServicios.getClientesPorIdUsuario(1L));
		// System.out.println("--------------FIN--------------");


		/* Usuario usuario = new Usuario("Nombre", "CUIT", "Domicilio", "Provincia", "Ciudad", "Condición Fiscal",
				LocalDate.now(), null, "usuario@email.com", "contraseña", null, null, null); */

	/* 	usServicios.guardarUsuario(usuario); */
//
//		 Usuario usDevuelto = usServicios.getUsuarioPorId(1L);
//		 System.out.println(usDevuelto);

	}
}
