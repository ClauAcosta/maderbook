package com.polo.MaderBook.controladores;

import com.polo.MaderBook.entidades.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.polo.MaderBook.servicios.ClienteServicios;

@RestController
@RequestMapping("/clientes")
public class ClienteControlador {

    @Autowired
    private ClienteServicios clienteServicios;

    @PostMapping("/crear")
    public Cliente crearCliente(@RequestBody Cliente cliente){
            return clienteServicios.guardarCliente(cliente);
    }
}
