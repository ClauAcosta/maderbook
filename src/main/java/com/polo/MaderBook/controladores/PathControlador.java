package com.polo.MaderBook.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PathControlador {

   @GetMapping("/")
   public String home() {

      return "index.html";
   }

   @Controller
   public class LoginControlador {

      @GetMapping("/ingresar")
      public String ingreso() {

         return "ingresar.html";
      }

   }

   @Controller
   public class TablasControlador {

      @GetMapping("/tables")
      public String Tablas() {

         return "tables.html";
      }

   }

   @Controller
   public class PerfilControlador {

      @GetMapping("/perfil")
      public String Perfil() {

         return "perfil.html";
      }

}}
