package com.polo.MaderBook.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.polo.MaderBook.dto.UsuarioRegistroDTO;
import com.polo.MaderBook.entidades.Usuario;
import com.polo.MaderBook.servicios.UsuarioServicio;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Controller
@RequestMapping("/register")
public class UsuarioControlador {

    @Autowired
    private UsuarioServicio usuarioServicio;

    @ModelAttribute("usuario")
    public Usuario usuario() {
        return new Usuario();
    }

    @GetMapping
    public String mostrarFormulariodeRegistro() {

        return "register.html";
    }

    @PostMapping
    public String registrarCuentadeUsuario(@ModelAttribute("usuario") Usuario usuario) {
        
         usuarioServicio.guardarUsuario(usuario);
        return "redirect:/register?exito";
    }
}
