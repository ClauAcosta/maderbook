/* package com.polo.MaderBook.servicios;

import java.time.LocalDate;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.polo.MaderBook.dto.UsuarioRegistroDTO;
import com.polo.MaderBook.entidades.Rol;
import com.polo.MaderBook.entidades.Usuario;
import com.polo.MaderBook.repositorios.UsuarioRepositorio;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Service
public class UsuarioServicioImpl implements UsuarioServicioLogin {

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Override
    public Usuario guardar(UsuarioRegistroDTO registroDTO) {
        Usuario usuario = new Usuario(registroDTO.getNombre(), registroDTO.getCuit(), registroDTO.getDomicilio(),
                registroDTO.getProvincia(), registroDTO.getCiudad(), registroDTO.getCondicionFiscal(), LocalDate.now(),
                registroDTO.getEmail(), registroDTO.getContrasena(), Arrays.asList(new Rol("ROLE_USER")), null, null);
        return usuarioRepositorio.save(usuario);
    }

} */
