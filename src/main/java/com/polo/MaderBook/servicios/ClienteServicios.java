package com.polo.MaderBook.servicios;

import com.polo.MaderBook.entidades.Cliente;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.polo.MaderBook.repositorios.ClienteRepositorio;
import java.util.NoSuchElementException;

@Service
public class ClienteServicios {

    @Autowired
    private ClienteRepositorio clienteRepositorio;

    @Transactional
    public Cliente guardarCliente(Cliente cliente) {

        return clienteRepositorio.save(cliente);
    }


    /*no devuelve todos los datos de la columna, solo los de cliente.*/
//    @Transactional
//    public List<Cliente> getClientes() {
//        return clienteRepositorio.findAll();
//    }

    @Transactional
    public Cliente getClientePorId(Long id) {
        return clienteRepositorio.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Cliente no encontrado con ID: " + id));
    }


    /*Actualizar Cliente (solo email por el momento)*/

    /*Eliminar toda la lista*/
    @Transactional
    public void eliminarClientes(){
        clienteRepositorio.deleteAll();
    }

    /*Eliminar individual*/
    @Transactional
    public void eliminarClientePorId(Long id){
        clienteRepositorio.deleteById(id);
    }


}
