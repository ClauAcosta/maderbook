package com.polo.MaderBook.servicios;

import com.polo.MaderBook.entidades.Cliente;
import com.polo.MaderBook.entidades.ProdMayorista;
import com.polo.MaderBook.entidades.Usuario;
import com.polo.MaderBook.repositorios.ClienteRepositorio;
import com.polo.MaderBook.repositorios.UsuarioRepositorio;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class UsuarioServicio {
    @Autowired
    private UsuarioRepositorio usuarioRepositorio;
    @Autowired
    private ClienteRepositorio clienteRepositorio;

    @Autowired
    private ProdMayoristaServicios prodMayoristaServicios;

    private static final Logger logger = LoggerFactory.getLogger(UsuarioServicio.class);


    @Transactional
    public Usuario guardarUsuario(Usuario usuario) {

        return usuarioRepositorio.save(usuario);
    }

    /* Servicios para buscar */
    // @Transactional
    // public List<Usuario> getUsuario() {
    // return usuarioRepositorio.findAll();
    // }

    @Transactional
    public Usuario getUsuarioPorId(Long id) {
        return usuarioRepositorio.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Usuario no encontrado con ID: " + id));
    }


    /*Servicios para manejar Productos mayoristas desde usuario*/
    //Agarra un producto ya creado y lo relaciona con un usuario.
    @Transactional
    public void agregarProductoAUsuario(Long usuarioId, ProdMayorista producto) {
        logger.info("Agregando producto mayorista al usuario con ID: {}", usuarioId);
        Usuario usuario = usuarioRepositorio.findById(usuarioId)
                .orElseThrow(() -> new NoSuchElementException("Usuario no encontrado con ID: " + usuarioId));
        producto.setUsuario(usuario);
        prodMayoristaServicios.guardarProdMayorista(producto);
        logger.info("Producto mayorista agregado exitosamente y relacionado con el usuario.");
    }
    //Elimina producto
    @Transactional
    public void eliminarProductoDeUsuario(Long usuarioId, Long productoId) {
        // Obtener el usuario por su ID
        Usuario usuario = usuarioRepositorio.findById(usuarioId)
                .orElseThrow(() -> new NoSuchElementException("Usuario no encontrado con ID: " + usuarioId));

        // Verificar si el usuario tiene el producto con el ID especificado
        boolean productoEncontrado = usuario.getProductos().stream()
                .anyMatch(producto -> producto.getId().equals(productoId));

        if (!productoEncontrado) {
            throw new NoSuchElementException("Producto no encontrado con ID: " + productoId);
        }

        // Llamar al método de eliminación de producto en la capa de servicios de ProdMayorista
        prodMayoristaServicios.eliminarMayoristaPorId(productoId);

        // Eliminar el producto de la lista de productos del usuario
        usuario.getProductos().removeIf(producto -> producto.getId().equals(productoId));

        // Actualizar el usuario en la base de datos para reflejar la eliminación del producto
        usuarioRepositorio.save(usuario);
    }

    //OBtener lista de productos vinculados al usuario
    //Devuelve la lista pero al mostrar el usuario muestra el objeto entero, modificar para q muestre id nomas.
    public List<ProdMayorista> getProductosDeUsuario(Long usuarioId) {
        // Obtener el usuario por su ID
        Usuario usuario = usuarioRepositorio.findById(usuarioId)
                .orElseThrow(() -> new NoSuchElementException("Usuario no encontrado con ID: " + usuarioId));

        // Obtener la lista de productos relacionados con el usuario
        return usuario.getProductos();
    }



    /*Servicios para manejar clientes desde usuario*/
    /*Get clientes por id de usuario*/

    public void eliminarUsuarioPorId(Long id) {
        usuarioRepositorio.deleteById(id);
    }

    /* Get clientes por id de usuario */

    @Transactional
    public List<Cliente> getClientesPorIdUsuario(Long idUsuario) {
        Usuario usuario = usuarioRepositorio.findById(idUsuario)
                .orElseThrow(() -> new NoSuchElementException("Usuario no encontrado con ID: " + idUsuario));

        return clienteRepositorio.findAllByUsuario(usuario);
    }

}
