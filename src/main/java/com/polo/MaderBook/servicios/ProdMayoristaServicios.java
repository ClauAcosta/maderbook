package com.polo.MaderBook.servicios;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.polo.MaderBook.entidades.ProdMayorista;
import com.polo.MaderBook.repositorios.ProdMayoristaRepositorio;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class ProdMayoristaServicios {
    private static final Logger logger = LoggerFactory.getLogger(ProdMayoristaServicios.class);

    @Autowired
    private ProdMayoristaRepositorio prodMayoristaRepositorio;

    @Transactional
    public void guardarProdMayorista(ProdMayorista prodMayorista) {
        logger.info("Guardando producto mayorista en la base de datos.");
        ProdMayorista productoGuardado = prodMayoristaRepositorio.save(prodMayorista);
        logger.info("Producto mayorista guardado exitosamente. ID: {}", productoGuardado.getId());
    }

    @Transactional
    public Iterable<ProdMayorista> getTodoMayorista() {
        return prodMayoristaRepositorio.findAll();
    }

    @Transactional
    public ProdMayorista getProdMayoristaPorId(Long id) {
        return prodMayoristaRepositorio.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Producto Mayorista no encontrado con ID: " + id));
    }

    @Transactional
    public void eliminarTodoProdMayoristas() {
        logger.info("Eliminando todos los productos mayoristas de la base de datos.");
        prodMayoristaRepositorio.deleteAll();
        logger.info("Todos los productos mayoristas han sido eliminados.");
    }

    @Transactional
    public void eliminarMayoristaPorId(Long id) {
        logger.info("Eliminando producto mayorista con ID: {} de la base de datos.", id);
        prodMayoristaRepositorio.deleteById(id);
        logger.info("Producto mayorista eliminado exitosamente.");
    }

    @Transactional
    public void aumentarStock(Long id, int cantidad) {
        logger.info("Aumentando stock para el producto con ID: {} en {} unidades.", id, cantidad);
        ProdMayorista prodMayorista = getProdMayoristaPorId(id);
        prodMayorista.aumentarStock(cantidad);
        guardarProdMayorista(prodMayorista);
        logger.info("Stock aumentado exitosamente. Nuevo stock: {}", prodMayorista.getStock());
    }

    @Transactional
    public void reducirStock(Long id, int cantidad) {
        logger.info("Reduciendo stock para el producto con ID: {} en {} unidades.", id, cantidad);
        ProdMayorista prodMayorista = getProdMayoristaPorId(id);
        prodMayorista.reducirStock(cantidad);
        if (prodMayorista.getStock() >= cantidad) {
            guardarProdMayorista(prodMayorista);
            logger.info("Stock reducido exitosamente. Nuevo stock: {}", prodMayorista.getStock());
        } else {
            logger.warn("No hay suficiente stock disponible para reducir en {} unidades.", cantidad);
            throw new IllegalArgumentException("No hay suficiente stock disponible.");
        }
    }


}
