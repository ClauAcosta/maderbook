package com.polo.MaderBook.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UsuarioRegistroDTO {

    private Long id;
    private String nombre;
    private String cuit;
    private String domicilio;
    private String provincia;
    private String ciudad;
    private String condicionFiscal;
    private String email;
    private String password;
    

}
