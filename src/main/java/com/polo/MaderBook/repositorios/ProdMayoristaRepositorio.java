package com.polo.MaderBook.repositorios;


import com.polo.MaderBook.entidades.ProdMayorista;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdMayoristaRepositorio extends CrudRepository<ProdMayorista, Long> {
    @Query("SELECT MAX(p.id) FROM ProdMayorista p")
    Long findMaxId();
}
