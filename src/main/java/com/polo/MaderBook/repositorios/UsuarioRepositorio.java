package com.polo.MaderBook.repositorios;

import com.polo.MaderBook.entidades.Usuario;


import org.springframework.data.repository.CrudRepository;


public interface UsuarioRepositorio extends CrudRepository<Usuario, Long> {

    Usuario findByEmail(String email);

    boolean existsByEmail(String email);
    
}
