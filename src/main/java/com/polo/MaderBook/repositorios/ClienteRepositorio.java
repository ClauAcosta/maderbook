package com.polo.MaderBook.repositorios;

import com.polo.MaderBook.entidades.Cliente;
import com.polo.MaderBook.entidades.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepositorio extends CrudRepository<Cliente, Long> {
    List<Cliente> findAllByUsuario(Usuario usuario);
}
